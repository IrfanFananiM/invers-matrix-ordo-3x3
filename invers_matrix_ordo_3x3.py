import os

a=1
while a==1:
	print("=====Aplikasi Pencari Invers Matriks=====")
	
	a=int(input("Masukkan Nilai A = "))
	b=int(input("Masukkan Nilai B = "))
	c=int(input("Masukkan Nilai C = "))
	d=int(input("Masukkan Nilai D = "))
	e=int(input("Masukkan Nilai E = "))
	f=int(input("Masukkan Nilai F = "))
	g=int(input("Masukkan Nilai G = "))
	h=int(input("Masukkan Nilai H = "))
	i=int(input("Masukkan Nilai I = "))

#Determinan
	detA=(a*e*i)+(b*f*g)+(c*d*h)-(g*e*c)-(h*f*a)-(i*d*b)
	print("\n=====MATRIKS A=====")
	print("|`",a," ",b," ",c,"`|")
	print("| ",d," ",e," ",f," |  ===> DetA = ",detA)
	print("|_",g," ",h," ",i,"_|")
	print("===============================")

#Adjoin
	a11=(e*i)-(h*f)
	a12=(d*i)-(g*f)
	a13=(d*h)-(g*e)
	a21=(b*i)-(h*c)
	a22=(a*i)-(g*c)
	a23=(a*h)-(g*b)
	a31=(b*f)-(e*c)
	a32=(a*f)-(d*c)
	a33=(a*e)-(d*b)
	print('=====ADJOIN======')
	print("A11 = (+)| ",e,"",f," |=",a11," A12 = (-)| ",d,"",f," |=",a12," A13 = (+)| ",d,"",e," |=",a13)
	print("         | ",h,"",i," |              | ",g,"",i," |              | ",g,"",h," |")

	print("\nA21 = (-)| ",b,"",c," |=",a21," A22 = (+)| ",a,"",c," |=",a22," A23 = (-)| ",a,"",b," |=",a23)
	print("         | ",h,"",i," |              | ",g,"",i," |              | ",g,"",h," |")

	print("\nA31 = (+)| ",b,"",c," |=",a31," A32 = (-)| ",a,"",c," |=",a32," A33 = (+)| ",a,"",b," |=",a33)
	print("         | ",e,"",f," |               | ",d,"",f," |              | ",d,"",e," |")

#printAdjoin
	print("==============================================================================")
	print("Adj = | ",a11*(1),"",a12*(-1),"",a13*(1)," |")
	print("      | ",a21*(-1),"",a22*(1),"",a23*(-1)," |")
	print("      | ",a31*(1),"",a32*(-1),"",a33*(1)," |")
	print("")
	print("Adjoin di Transpose menjadi = ")
	print("A Traspose = | ",a11*(1),'',a21*(-1),'',a31*(1),' |')
	print('             | ',a12*(-1),'',a22*(1),'',a32*(-1),' |')
	print('             | ',a13*(1),'',a23*(-1),'',a33*(1),' |')
	print("==============================================================================")

#invers
	ina11=(1/detA*(a11*(1)))
	ina12=(1/detA*(a12*(-1)))
	ina13=(1/detA*(a13*(1)))
	ina21=(1/detA*(a21*(-1)))
	ina22=(1/detA*(a22*(1)))
	ina23=(1/detA*(a23*(-1)))
	ina31=(1/detA*(a31*(1)))
	ina32=(1/detA*(a32*(-1)))
	ina33=(1/detA*(a33*(1)))

	print("\nA-1 = 1/Det A x Adj A")
	print('    = 1/',detA,"| ",a11,'',a21,'',a31," |")
	print('            | ',a12,'',a22,'',a32,' |')
	print('            | ',a13,'',a23,'',a33,' |')

	print('\n    = | ',ina11,'',ina21,'',ina31,' |')
	print('      | ',ina12,'',ina22,'',ina32,' |')
	print('      | ',ina13,'',ina23,'',ina33,' |')

	input("\nPress Enter to Continue!")
	os.system('cls')